#ifndef _ENGINE_HPP_
#define _ENGINE_HPP_

#include <deque>
#include <memory>
#include <pthread.h>
#include <set>
#include <string>

#include "ErrType.hpp"
#include "PcmConverter.hpp"

// The engine instance owns a filequeue and manages worker threads.

class IEngine
{

public:
    virtual ~IEngine() = default;

    /// Converts all Wav files in the given In folder to Mp3 ones
    /// in the give Out folder, Out == In if Out is empty.
    virtual ErrType processFolder(const std::string& in_dir,
                                  const std::string& out_dir = "") = 0;

protected:
    /// Scans the folder and adds every *.wav file name to the
    /// file queue.
    virtual ErrType enqueueAllWavsFrom(const std::string& folder) = 0;

    /// Opens each file from the member queue, creates an *.mp3 file with the
    /// same name in the given folder, converts PCM to MP3 and writes to mp3
    /// file.
    virtual ErrType convertQueuedFilesTo(const std::string& folder) = 0;
};

class Engine : public IEngine
{

public:
    explicit Engine();
    virtual ErrType processFolder(const std::string& in_dir,
                                  const std::string& out_dir = "") override;

private:
    virtual ErrType enqueueAllWavsFrom(const std::string& folder) override;

    virtual ErrType convertQueuedFilesTo(const std::string& folder) override;

    /// Returns true if the filename extension is ".wav"
    bool looksLikeWav(const std::string& filename);

private:
    std::deque<std::string> m_file_queue;
    pthread_mutex_t m_file_queue_mutex = PTHREAD_MUTEX_INITIALIZER;

    std::set<uintptr_t> m_workers_set;
    pthread_mutex_t m_workers_set_mutex = PTHREAD_MUTEX_INITIALIZER;

    std::string m_input_dir;
    std::string m_output_dir;

    static constexpr size_t MINIMUM_CPU_CORES_POSSIBLE = 1;
    size_t m_total_cores = MINIMUM_CPU_CORES_POSSIBLE;
};

#endif // _ENGINE_H_
