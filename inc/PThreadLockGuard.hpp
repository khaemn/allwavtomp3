#ifndef PTHREADLOCKGUARD_H
#define PTHREADLOCKGUARD_H

#include <pthread.h>

/// A primitive helper class to implement RAII for a pthread mutex
class PThreadLockGuard
{
public:
    explicit PThreadLockGuard(pthread_mutex_t& mutex)
        : m_mutex(mutex)
    {
        lock();
    }
    ~PThreadLockGuard() { release(); }
    void release()
    {
        if (m_is_locked) {
            m_is_locked = (pthread_mutex_unlock(&m_mutex) != 0);
        }
    }
    bool lock()
    {
        if (!m_is_locked) {
            m_is_locked = (pthread_mutex_lock(&m_mutex) == 0);
        }
        return m_is_locked;
    }

private:
    pthread_mutex_t& m_mutex;
    bool m_is_locked = false;
};

#endif // PTHREADLOCKGUARD_H
