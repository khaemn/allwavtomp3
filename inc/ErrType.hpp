#ifndef ERRTYPE_H
#define ERRTYPE_H

#include <ostream>

enum class ErrType : int
{
    OK = 0,
    FAILED,
    INVALID_PCM_FILE,
    FILE_IO_ERROR,
    UNSUPPORTED_PCM_FORMAT,
    UNSUPPORTED_SAMPLERATE,
    UNSUPPORTED_CHANNEL_COUNT,
};

std::ostream& operator<<(std::ostream& os, const ErrType& error);

#endif // ERRTYPE_H
