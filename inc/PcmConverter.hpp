#ifndef _PCM_CONVERTER_HPP_
#define _PCM_CONVERTER_HPP_

#include <array>
#include <string>

#include "ErrType.hpp"
#include "lame.h"

// The PcmConverter class implements converting Pcm file to another format
class IPcmConverter
{
public:
    virtual ~IPcmConverter() = default;

    /// Opens inpath as readonly, outpath as writeonly, performs PCM
    /// file parsing and conversion to another format, closes both files.
    /// In case of any error, both file handles are guaranteed to be closed.
    virtual ErrType convert(const std::string& inpath,
                            const std::string& outpath) const = 0;

    static constexpr size_t PCM_HEADER_SIZE_BYTES = 40;
    static constexpr uint16_t MIN_CHANNELS_SUPPORTED = 1;
    static constexpr uint16_t MAX_CHANNELS_SUPPORTED = 2;
    static constexpr uint16_t PCM_BIT_DEPTH_SUPPORTED = 16;
    static constexpr uint16_t MIN_SAMPLERATE_SUPPORTED = 8000;
    static constexpr uint16_t MAX_SAMPLERATE_SUPPORTED = 44100;
};

using PcmHeader = std::array<char, IPcmConverter::PCM_HEADER_SIZE_BYTES>;

struct PcmProperties
{
    uint16_t channels;
    uint32_t samplerate_hz;
    uint16_t bit_depth;
};

class Mp3Converter : public IPcmConverter
{
public:
    virtual ErrType convert(const std::string& wavpath,
                            const std::string& mp3path) const override;

    static ErrType parsePcmHeader(const PcmHeader& header,
                                  PcmProperties& props);
};

#endif // _PCM_CONVERTER_HPP_
