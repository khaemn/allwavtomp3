#ifndef _HARDWARE_HPP_
#define _HARDWARE_HPP_

// This file contains only 1 platform-dependent function,
// that returns number of available CPU cores.
// In C++11 threads there is a portable and convenient
// std::thread::hardware_concurrency(),
// however only pthreads are used in this project, and therefore
// the trick was to workaround usage of C++11 threads.
// Original code taken from
// https://stackoverflow.com/questions/150355/programmatically-find-the-number-of-cores-on-a-machine

#ifdef _WIN32
#include <windows.h>
#elif MACOS
#include <sys/param.h>
#include <sys/sysctl.h>
#else
#include <unistd.h>
#endif

namespace HardwareDependent {
unsigned int available_cpu_cores()
{
#ifdef WIN32
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    return sysinfo.dwNumberOfProcessors;
#elif MACOS
    int nm[2];
    size_t len = 4;
    uint32_t count;

    nm[0] = CTL_HW;
    nm[1] = HW_AVAILCPU;
    sysctl(nm, 2, &count, &len, NULL, 0);

    if (count < 1) {
        nm[1] = HW_NCPU;
        sysctl(nm, 2, &count, &len, NULL, 0);
        if (count < 1) {
            count = 1;
        }
    }
    return count;
#else
    return sysconf(_SC_NPROCESSORS_ONLN);
#endif
}
} // namespace HardwareDependent
#endif // _HARDWARE_H_
