#include "PcmConverter.hpp"

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

#include "ErrType.hpp"
#include "lame_global_flags.h"

using std::cout;
using std::endl;
using std::string;
using std::unique_ptr;
using std::vector;

template <typename IntType>
static inline IntType getInteger(PcmHeader::const_iterator lowest)
{
    static constexpr auto BYTE_SIZE = 8;
    IntType result = 0;
    for (size_t i = 0; i < sizeof(IntType); i++) {
        result += static_cast<IntType>(static_cast<unsigned char>(*lowest)
                                       << (BYTE_SIZE * i));
        std::advance(lowest, 1);
    }
    return result;
}

ErrType Mp3Converter::convert(const std::string& wavpath,
                              const std::string& mp3path) const
{
    unique_ptr<FILE, int (*)(FILE*)> pcm_file(fopen(wavpath.c_str(), "rb"),
                                              fclose);
    if (pcm_file == nullptr) {
        cout << endl << "Can not open " << wavpath << " for reading!" << endl;
        return ErrType::FILE_IO_ERROR;
    }

    unique_ptr<FILE, int (*)(FILE*)> mp3_file(fopen(mp3path.c_str(), "wb"),
                                              fclose);
    if (mp3_file == nullptr) {
        cout << endl << "Can not open " << mp3path << " for writing!" << endl;
        return ErrType::FILE_IO_ERROR;
    }

    PcmHeader pcm_header;
    const auto header_len = fread(pcm_header.data(), sizeof(char),
                                  PCM_HEADER_SIZE_BYTES, pcm_file.get());

    if (header_len < PCM_HEADER_SIZE_BYTES) {
        cout << endl << wavpath << " is not a valid PCM file!" << endl;
        return ErrType::INVALID_PCM_FILE;
    }

    PcmProperties pcmProps;
    const auto result = parsePcmHeader(pcm_header, pcmProps);
    if (result != ErrType::OK) {
        return result;
    }
    const bool isMono = pcmProps.channels == 1;

    // LAME examples use stack arrays of size 8192, however, to decrease IO
    // operations and prevent stack bloating I enlarged the buffers and moved
    // them to heap. 2 x ~1-2Mb memory buffers for each processing thread is
    // quite small, considering even embedded platforms such as Raspberry Pi.
    static constexpr size_t PCM_BUFFER_SIZE = 8192 * 128;
    static constexpr size_t MP3_BUFFER_SIZE = PCM_BUFFER_SIZE;

    vector<short int> pcm_buffer;
    pcm_buffer.resize(PCM_BUFFER_SIZE * sizeof(short int), 0);

    vector<unsigned char> mp3_buffer;
    mp3_buffer.resize(MP3_BUFFER_SIZE, 0);

    // According to LAME documentation, an instance of encoder should be
    // inited for each separate file conversion.
    unique_ptr<lame_global_flags, int (*)(lame_global_flags*)> lame(lame_init(),
                                                                    lame_close);
    lame_set_in_samplerate(lame.get(),
                           static_cast<int>(pcmProps.samplerate_hz));
    lame_set_VBR(lame.get(), vbr_default);
    lame_set_mode(lame.get(), isMono ? MPEG_mode_e::MONO : MPEG_mode_e::STEREO);
    lame_init_params(lame.get());

    // To eliminate an audible artifact, a PCM conversion is better to be
    // started not from the very first sample.
    fseek(pcm_file.get(),
          PCM_HEADER_SIZE_BYTES + pcmProps.channels * sizeof(uint16_t), 0);

    int write(0);
    size_t read(0);
    do {
        read = fread(pcm_buffer.data(), pcmProps.channels * sizeof(uint16_t),
                     PCM_BUFFER_SIZE, pcm_file.get());

        if (0 == read) {
            write = lame_encode_flush(lame.get(), mp3_buffer.data(),
                                      MP3_BUFFER_SIZE);
        } else {
            write =
                isMono
                    ? lame_encode_buffer(lame.get(), pcm_buffer.data(),
                                         pcm_buffer.data(),
                                         static_cast<int>(read),
                                         mp3_buffer.data(), MP3_BUFFER_SIZE)
                    : lame_encode_buffer_interleaved(
                          lame.get(), pcm_buffer.data(), static_cast<int>(read),
                          mp3_buffer.data(), MP3_BUFFER_SIZE);
        }

        if (write < 0) {
            cout << "Unrecoverable LAME failure while converting" << mp3path
                 << ", operation aborted." << endl;
            return ErrType::FAILED;
        }

        fwrite(mp3_buffer.data(), static_cast<size_t>(write),
               sizeof(unsigned char), mp3_file.get());

    } while (read != 0);

    return ErrType::OK;
}

ErrType Mp3Converter::parsePcmHeader(const PcmHeader& header,
                                     PcmProperties& props)
{
    // Offsets and other constants according to RIFF header description at
    // http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html
    static constexpr size_t WAVE_TOKEN_OFFSET = 8;
    static constexpr size_t WAVE_BIT_DEPTH_OFFSET = 16;
    static constexpr size_t WAVE_FORMAT_OFFSET = 20;
    static constexpr size_t CHANNELS_OFFSET = WAVE_FORMAT_OFFSET + 2;
    static constexpr size_t SAMPLERATE_HZ_OFFSET = CHANNELS_OFFSET + 2;
    static constexpr unsigned short WAVE_FORMAT_PCM = 0x0001;

    static const vector<char> RIFF = { 'R', 'I', 'F', 'F' };
    static const vector<char> WAVE = { 'W', 'A', 'V', 'E' };

    const bool isRiff = std::equal(RIFF.begin(), RIFF.end(), header.begin());

    const bool isWave = std::equal(WAVE.begin(), WAVE.end(),
                                   header.begin() + WAVE_TOKEN_OFFSET);

    if (!isRiff || !isWave) {
        return ErrType::INVALID_PCM_FILE;
    }

    uint16_t bits =
        getInteger<uint16_t>(header.cbegin() + WAVE_BIT_DEPTH_OFFSET);
    if (bits != PCM_BIT_DEPTH_SUPPORTED) {
        return ErrType::UNSUPPORTED_PCM_FORMAT;
    }
    props.bit_depth = bits;

    uint16_t format =
        getInteger<uint16_t>(header.cbegin() + WAVE_FORMAT_OFFSET);

    if (format != WAVE_FORMAT_PCM) {
        return ErrType::UNSUPPORTED_PCM_FORMAT;
    }

    uint16_t channels = getInteger<uint16_t>(header.cbegin() + CHANNELS_OFFSET);

    if (channels < MIN_CHANNELS_SUPPORTED ||
        channels > MAX_CHANNELS_SUPPORTED) {
        return ErrType::UNSUPPORTED_CHANNEL_COUNT;
    }
    props.channels = channels;

    uint32_t samplerate =
        getInteger<uint32_t>(header.cbegin() + SAMPLERATE_HZ_OFFSET);
    if (samplerate < MIN_SAMPLERATE_SUPPORTED ||
        samplerate > MAX_SAMPLERATE_SUPPORTED) {
        return ErrType::UNSUPPORTED_SAMPLERATE;
    }
    props.samplerate_hz = samplerate;

    return ErrType::OK;
}
