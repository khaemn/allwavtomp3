#include "ErrType.hpp"

std::ostream& operator<<(std::ostream& os, const ErrType& error)
{
    switch (error) {
        case ErrType::OK:
            os << "OK";
            break;
        case ErrType::FAILED:
            os << "FAILED";
            break;
        case ErrType::INVALID_PCM_FILE:
            os << "INVALID_PCM_FILE";
            break;
        case ErrType::FILE_IO_ERROR:
            os << "FILE_IO_ERROR";
            break;
    }
    return os;
}
