#include "Engine.hpp"

#include <algorithm>
#include <cerrno>
#include <chrono>
#include <dirent.h>
#include <iostream>
#include <math.h>
#include <unistd.h>
#include <vector>

#include "Hardware.hpp"

#include "PThreadLockGuard.hpp"

// TODO: move as a static member to Engine?
static const std::string wav_ending = ".wav";
static const std::string mp3_ending = ".mp3";

using std::cout;
using std::endl;
using std::string;

struct ThreadArgs
{
    std::deque<std::string>& filename_queue;
    pthread_mutex_t& filename_mutex;

    std::set<uintptr_t>& workers;
    pthread_mutex_t& workers_mutex;

    const std::string& in_dir;
    const std::string& out_dir;
    const IPcmConverter& converter;
};

void* conversionThreadFunction(void* args)
{
    ThreadArgs* _args = static_cast<ThreadArgs*>(args);
    PThreadLockGuard work_mutex(_args->workers_mutex);
    _args->workers.insert(pthread_self());
    work_mutex.release();

    while (_args->filename_queue.size() > 0) {
        PThreadLockGuard filename_mutex(_args->filename_mutex);
        auto filename = _args->filename_queue.front();
        _args->filename_queue.pop_front();
        filename_mutex.release();

        std::string mp3path = _args->out_dir + "/" + filename + mp3_ending;
        std::string wavpath = _args->in_dir + "/" + filename + wav_ending;

        auto error = _args->converter.convert(wavpath, mp3path);
        if (error != ErrType::OK) {
            cout << "Error " << error << " while converting " << filename
                 << wav_ending << endl;
            // Each mp3 file by design is accessed by only 1 thread, no locks
            // needed here.
            remove(mp3path.c_str());
        }
    }

    work_mutex.lock();
    _args->workers.erase(pthread_self());
    work_mutex.release();
    return nullptr;
}

Engine::Engine()
    : m_total_cores(
          static_cast<size_t>(HardwareDependent::available_cpu_cores()))
{
    cout << "Found " << m_total_cores << " CPU cores.\n";
}

ErrType Engine::processFolder(const std::string& in_dir,
                              const std::string& out_dir)
{
    m_output_dir = out_dir.empty() ? in_dir : out_dir;
    m_input_dir = in_dir;

    enqueueAllWavsFrom(m_input_dir);

    return convertQueuedFilesTo(m_output_dir);
}

ErrType Engine::enqueueAllWavsFrom(const std::string& folder)
{
    cout << "\nEnqueuing files from\n" << folder << "\n";

    std::unique_ptr<DIR, int (*)(DIR*)> dir(opendir(folder.c_str()), closedir);

    if (dir.get() == nullptr) {
        cout << "Can not open folder " << folder << ", error code " << errno
             << endl;
        return ErrType::FILE_IO_ERROR;
    }

    struct dirent* entry(nullptr);
    while ((entry = readdir(dir.get())) != nullptr) {

        const std::string filename(entry->d_name);

        if (looksLikeWav(filename)) {
            const string bare_name(
                filename.substr(0, filename.size() - wav_ending.size()));

            m_file_queue.push_back(std::move(bare_name));

            cout << "    enqueued " << entry->d_name << endl;
        }
    }

    cout << "Folder\n"
         << folder << "\nsuccessfully scanned, total " << m_file_queue.size()
         << " wav files found to process.\n";
    return ErrType::OK;
}

ErrType Engine::convertQueuedFilesTo(const std::string& folder)
{
    if (m_file_queue.empty()) {
        cout << "\nEmpty processing queue, no files to convert.\n";
        return ErrType::OK;
    }

    cout << "\nConverting files ...\n";

    const auto start_time = std::chrono::steady_clock::now();

    const size_t total_threads = std::min(m_total_cores, m_file_queue.size());
    const unsigned int STATUS_UPDATE_TIME_S =
        std::max(5 / total_threads, size_t(1));

    std::vector<std::unique_ptr<pthread_t>> converting_threads;
    std::vector<std::shared_ptr<const IPcmConverter>> converters;

    auto prev_files_left = m_file_queue.size();
    size_t prev_workers = 0;
    for (size_t i = 0; i < total_threads; i++) {
        converting_threads.emplace_back(new pthread_t);
        converters.emplace_back(new Mp3Converter);
        ThreadArgs conversionArgs{ m_file_queue,  m_file_queue_mutex,
                                   m_workers_set, m_workers_set_mutex,
                                   m_input_dir,   folder,
                                   *converters[i] };
        pthread_create(converting_threads[i].get(), nullptr,
                       &conversionThreadFunction, &conversionArgs);
        pthread_detach(*converting_threads[i]);
    }

    while (!m_file_queue.empty() || !m_workers_set.empty()) {
        const auto files_left = m_file_queue.size() + m_workers_set.size();
        if (prev_files_left == files_left) {
            sleep(STATUS_UPDATE_TIME_S);
            continue;
        }
        prev_files_left = files_left;
        cout << m_file_queue.size() << " more file(s) left to process, "
             << m_workers_set.size() << " converter(s) now working." << endl;
    };

    const auto end_time = std::chrono::steady_clock::now();

    const auto time_diff_ms =
        std::chrono::duration_cast<std::chrono::milliseconds>(end_time -
                                                              start_time)
            .count();

    cout << "Time elapsed: " << time_diff_ms << " ms ("
         << round(time_diff_ms / 1000.0) << " seconds)\n";

    return ErrType::OK;
}

bool Engine::looksLikeWav(const std::string& filename)
{
    if (filename.size() < wav_ending.size() + 1) {
        return false;
    }
    std::string ending =
        filename.substr(filename.size() - wav_ending.size(), wav_ending.size());
    return ending == wav_ending;
}
