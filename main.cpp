#include <iostream>
#include <string>

#include <Engine.hpp>

int main(int argc, char* argv[])
{
    Engine engine;

    std::string dir_path = argv[0];
#ifdef WIN32
    size_t pos = dir_path.find_last_of("\\") + 1;
#else
    size_t pos = dir_path.find_last_of("/") + 1;
#endif
    dir_path.erase(pos, dir_path.size() - pos);
    std::cout << "\nWorking directory:\n    " << dir_path << std::endl;

    if (argc >= 2) {
        engine.processFolder(dir_path, argv[1]);
    } else {
        engine.processFolder(dir_path);
    }

    return 0;
}
